package presles;
/**
 * @author Moulinux
 * @version 0.1
 * @copyright Copyright 2019 par Moulinux. Tous droits réservés.
 * @license GPLv3, voir la LICENSE pour plus d'informations
 *
 * @mainpage Page d'accueil
 * @section intro_sec Introduction
 * La calculette éco-déplacement est une application qui permet d’évaluer
 * les impacts environnementaux et économiques des différents modes de transport
 * et permet de comparer les résultats entre deux modes de déplacement pour un même trajet
 * @section install_sec Installation
 * @subsection step1 Préparation du système
 * Sur une distribution Debian, il suffit d'installer les paquets suivants :
 * ```
 * apt install default-jdk openjfx maven
 * ```
 * > Pour les autres systèmes, je recommande d'adopter [OpenJDK] et [OpenJFX]...
 * [OpenJDK]: https://adoptopenjdk.net/
 * [OpenJFX]: https://gluonhq.com/products/javafx/
 * @subsection step2 Exécuter l'application
 * Une fois le projet cloné, ou téléchargé :
 * ```
 * cd CedFx
 * mvn clean javafx:run
 * ```
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import presles.Modele.Deplacement;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    private static ArrayList<Deplacement> lesDeplacements = new ArrayList();
    
    public static ArrayList<Deplacement> getDeplacements() {
        return lesDeplacements;
    }

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("Vue/comparateur"));
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}
