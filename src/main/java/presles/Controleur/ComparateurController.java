package presles.Controleur;

import presles.Modele.Deplacement;
import java.io.IOException;
import static java.lang.Math.abs;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import presles.App;
import presles.Config;

/**
 * FXML Controller class
 *
 * @author moulinux
 */
public class ComparateurController implements Initializable {
    @FXML
    private TextField distance;
    @FXML
    private ComboBox dep1;
    @FXML
    private ComboBox dep2;
    @FXML
    private Label co2Dep1;
    @FXML
    private Label co2Dep2;
    @FXML
    private Label energieDep1;
    @FXML
    private Label energieDep2;
    @FXML
    private Label co2Comp;
    @FXML
    private Label energieComp;
    @FXML
    private Button btnAjout;
    @FXML
    private Button btnDep;
    @FXML
    private Button btnComp;
    private Deplacement d1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ArrayList<String> listeModDep = new ArrayList();
        listeModDep.add("train");
        listeModDep.add("velo");
        listeModDep.add("voiture");
        dep1.setItems(FXCollections.observableList(listeModDep));
        dep2.setItems(FXCollections.observableList(listeModDep));
    }
    
    @FXML
    private void selectionDep1(ActionEvent event) {
        String selection = dep1.getSelectionModel().getSelectedItem().toString();
        afficheConsole("premier", selection);
        calcule();
    }

    @FXML
    private void selectionDep2(ActionEvent event) {
        String selection = (String)dep2.getSelectionModel().getSelectedItem();
        afficheConsole("second", selection);
        calcule();
    }
    
    @FXML
    private void saisieDistance(ActionEvent event) {
        String km = (String)distance.getText();
        System.out.println("Vous travaillez à : " + km + " km de votre domicile");
        calcule();
    }
    
    @FXML
    private void ajouterDep(ActionEvent event) {
        if (d1 != null) {
            App.getDeplacements().add(this.d1);
            System.out.println("Un déplacement vient d'être ajouté : " + this.d1);
            d1 = null;
            //désactivier le bouton
            btnAjout.setDisable(true);
        }
    }
    
    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException {
        if (event.getSource() == btnDep){
            BorderPane border = (BorderPane)btnDep.getScene().getRoot();

            URL paneOneUrl = getClass().getResource("/presles/Vue/deplacement.fxml");
            AnchorPane depPane = FXMLLoader.load( paneOneUrl );

            border.setCenter(depPane);
         }
        else {
            Stage stage = (Stage) btnComp.getScene().getWindow();
            Parent root = FXMLLoader.load(getClass().getResource("/presles/Vue/comparateur.fxml"));

            //créer une nouvelle scene pour l'associer au stage
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }
    }
    
    private void afficheConsole(String choix, String selection) {
        String message = "Vous avez choisi le %s mode de déplacement suivant : %s";
        System.out.println(String.format(message, choix, selection));
        ArrayList<Double> couts = (ArrayList<Double>)Config.getCoutDep().get(selection); //rechercher la correspondance dans le dictionnaire
        System.out.println("La liste des coûts associé est : " + couts);
    }
    
    private void calcule() {
        ArrayList<Double> listeCouts;
        String messCo2, messEnergie;
        
        if ((distance.getText() != null && !distance.getText().isEmpty()) &&
            (dep1.getValue() != null && !dep1.getValue().toString().isEmpty()) &&
            (dep2.getValue() != null && !dep2.getValue().toString().isEmpty())
        ) {
            //réactiver le bouton
            btnAjout.setDisable(false);
            
            Integer km = Integer.parseInt(distance.getText());
            this.d1 = new Deplacement(km, dep1.getSelectionModel().getSelectedItem().toString());
            Deplacement d2 = new Deplacement(km, dep2.getSelectionModel().getSelectedItem().toString());
            System.out.println(d1);
            System.out.println(d2);
            
            //affiche les coûts environnementaux
            listeCouts = (ArrayList<Double>)Config.getCoutDep().get(d1.getTransport());
            Double valCo2Dep1 = d1.getDistance()*listeCouts.get(0);
            co2Dep1.setText(String.format("%.2f kg eq. CO2", valCo2Dep1));
            listeCouts = (ArrayList<Double>)Config.getCoutDep().get(d2.getTransport());
            Double valCo2Dep2 = d2.getDistance()*listeCouts.get(0);
            co2Dep2.setText(String.format("%.2f kg eq. CO2", valCo2Dep2));
            
            //affiche les coûts énergétiques
            listeCouts = (ArrayList<Double>)Config.getCoutDep().get(d1.getTransport());
            Double valEnergieDep1 = d1.getDistance()*listeCouts.get(1);
            energieDep1.setText(String.format("%.2f l eq. pétrole", valEnergieDep1));
            listeCouts = (ArrayList<Double>)Config.getCoutDep().get(d2.getTransport());
            Double valEnergieDep2 = d2.getDistance()*listeCouts.get(1);
            energieDep2.setText(String.format("%.2f l eq. pétrole", valEnergieDep2));
            
            //compare les coûts
            Double diffCout = valEnergieDep1 - valEnergieDep2;
            if (diffCout < 0) {
                messCo2 = "J'évite %.2f kg\neq. CO2 par an";
                messEnergie = "Je consomme %.2f litres\neq. pétrole en moins par an";
            } else {
                messCo2 = "J'émets %.2f kg\neq. CO2 par an";
                messEnergie = "Je consomme %.2f litres\neq. pétrole en plus par an";                
            }
            co2Comp.setText(String.format(messCo2, abs(valCo2Dep1-valCo2Dep2)));
            energieComp.setText(String.format(messEnergie, abs(valEnergieDep1-valEnergieDep2)));
        }
    }
}