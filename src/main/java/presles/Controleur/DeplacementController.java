package presles.Controleur;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import presles.App;
import presles.Config;
import presles.Modele.Deplacement;

/**
 * FXML Controller class
 *
 * @author moulinux
 */
public class DeplacementController implements Initializable {
    @FXML
    private TableView deplacements;
    @FXML
    private Label co2Dep;
    @FXML
    private Label energieDep;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Ajout des colonnes à la TableView
        //http://docs.oracle.com/javafx/2/ui_controls/table-view.htm
        TableColumn distanceCol = new TableColumn("Distance");
        TableColumn transportCol = new TableColumn("Transport");
        distanceCol.setCellValueFactory(
            new PropertyValueFactory<Deplacement, Integer>("distance")
        );
        transportCol.setCellValueFactory(
            new PropertyValueFactory<Deplacement, String>("transport")
        );
        distanceCol.setMinWidth(50);
        transportCol.setMinWidth(100);
        this.deplacements.getColumns().add(distanceCol);
        this.deplacements.getColumns().add(transportCol);
        
        //alimenter la TableView à partir des données de la collection
        this.deplacements.setItems(FXCollections.observableArrayList(App.getDeplacements()));
        
        // Ajout d'un listener sur le TableView
        //http://stackoverflow.com/questions/13393301/read-selection-from-tableview-in-javafx-2-0
        deplacements.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            // la méthode est appelée à chaque fois qu'une ligne est sélectionnée
            @Override
            public void changed(ObservableValue observale, Object oldValue, Object newValue) {
                ArrayList<Double> listeCouts;
                Double cout;

                Deplacement d = (Deplacement) newValue;

                //affiche les coûts environnementaux
                listeCouts = (ArrayList<Double>)Config.getCoutDep().get(d.getTransport());
                cout = d.getDistance()*listeCouts.get(0);
                co2Dep.setText(String.format("J'émets %.2f kg eq. CO2 par an", cout));

                //affiche les coûts énergétiques
                listeCouts = (ArrayList<Double>)Config.getCoutDep().get(d.getTransport());
                cout = d.getDistance()*listeCouts.get(1);
                energieDep.setText(String.format("Je consomme %.2f litres eq. pétrole par an", cout));
        }
        });
    }    
    
}