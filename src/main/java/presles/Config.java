package presles;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author moulinux
 */
public class Config {
    
    private static HashMap<String,ArrayList<Double>> coutDep;
    
    public static HashMap<String,ArrayList<Double>> getCoutDep() {
        if (coutDep == null) {
            //liste des coûts pour le train
            ArrayList<Double> coutTrain = new ArrayList();
            coutTrain.add(14.62);
            coutTrain.add(9.26);
            //liste des coûts pour le vélo
            ArrayList<Double> coutVelo = new ArrayList();
            coutVelo.add(0.0);
            coutVelo.add(0.0);
            //liste des coûts pour la voiture
            ArrayList<Double> coutVoiture = new ArrayList();
            coutVoiture.add(129.62);
            coutVoiture.add(50.65);    
            //dictionaire destiné aux coûts
            coutDep = new HashMap();
            coutDep.put("train", coutTrain);
            coutDep.put("velo", coutVelo);
            coutDep.put("voiture", coutVoiture);
        }
        return coutDep;
    }
    
}