package presles.Modele;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Classe gérant les déplacements
 * @author moulinux
 */
public class Deplacement {
    
    private SimpleIntegerProperty distance;
    private SimpleStringProperty transport;

    /**
     * Constructeur de la classe
     *
     * @param distance
     * @param transport
     */
    public Deplacement(Integer distance, String transport) {
        this.distance = new SimpleIntegerProperty(distance);
        this.transport = new SimpleStringProperty(transport);
    }

    /**
     * accesseur de la distance
     *
     * @return entier correspondant à la distance
     */
    public Integer getDistance() {
        return distance.get();
    }

    /**
     * mutateur de la distance
     *
     * @param distance
     */
    public void setDistance(Integer distance) {
        this.distance.set(distance);
    }

    /**
     * accesseur du mode de transport
     *
     * @return chaîne de caractères correspondant au mode de transport
     */
    public String getTransport() {
        return transport.get();
    }

    /**
     * mutateur du mode de transport
     *
     * @param transport
     */
    public void setTransport(String transport) {
        this.transport.set(transport);
    }

    /**
     * accesseur de la propriété distance
     *
     * @return objet correspondant
     */
    public SimpleIntegerProperty distanceProperty() {
        return this.distance;
    }

    /**
     * accesseur de la propriété transport
     *
     * @return objet correspondant
     */
    public SimpleStringProperty transportProperty() {
        return this.transport;
    }

    /**
     * Représentation textuelle du déplacement
     *
     * @return chaîne de caractères
     */
    public String toString() {
        return "distance : " + this.getDistance()+
                "; transport : " + this.getTransport();
    }
}