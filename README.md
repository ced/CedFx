# CedFX : Calculette éco-déplacement

La calculette éco-déplacement est une application qui permet d’évaluer
les impacts environnementaux et économiques des différents modes de transport
et permet de comparer les résultats entre deux modes de déplacement pour un même trajet.
![Screenshot](./doc/img/Capture_CedFx_1.png)

Cette application est utilisée dans le cadre de ma progression pédagogique
avec les BTS SIO. Il existe plusieurs versions. Cette version repose sur le
*framework* JavaFX.

## Introduction
Cette application est inspirée de celle de l'[Ademe], qui n'était cependant pas libre
et encore moins interopérable (reposant notamment sur Flash). Cette version de
l'application propose de nombreuses itérations (accessibles via les différentes
branches du dépôt). L'itération principale n'est pas la plus aboutie, mais rend
la démonstration plus simple car les déplacements ajoutés, le sont dans une collection
en mémoire (et non sérialisés dans une base de données relationnelles).
![Screenshot](./doc/img/Capture_CedFx_2.png)

## Environnement technique
Il est bien sûr nécessaire d'installer au préalable sur votre système une
distribution du JDK et de JavaFX, ainsi que maven.

Sur une distribution Debian, il suffit d'installer les paquets suivants :
```
apt install default-jdk openjfx maven
```
> Pour les autres systèmes, je recommande d'adopter [OpenJDK] et [OpenJFX]...

Une fois le projet cloné, ou téléchargé :
```
cd CedFx
mvn clean javafx:run
```

## Documentation technique

La documentation est générée avec [Doxygen] via le moteur d'intégration continue
de gitlab et hébergée sur les [pages](https://ced.frama.io/CedFx) de Framagit.

## Distribution

L'application peut être distribuée en générant une archive, à l'aide de la commande:
```
mvn package
```
> Le fichier manifeste `meta-inf/manifest.mf` est alimenté par les données
présentes dans le fichier `pom.xml`.

Le fichier `jar` peut alors être distribué. La commande pour lancer l'application
à partir de cette archive est :
```
java -jar --module-path /usr/share/openjfx/lib --add-modules=javafx.controls,javafx.fxml CedFx-6.0-master.jar
```

[Ademe]: https://www.ademe.fr/
[OpenJDK]: https://adoptopenjdk.net/
[OpenJFX]: https://gluonhq.com/products/javafx/
[Doxygen]: http://www.doxygen.nl/
